﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_autoclave
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = true;
            tmrCerrar.Enabled = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            tmrCerrar.Enabled = true;
            tmrAbrir.Enabled = false;
        }

        private void tmrAbrir_Tick(object sender, EventArgs e)
        {
            // Este es el evento del botón de abrir la puerta
            if (panelPuerta.Height > 0)
            {
                panelPuerta.Height--; // decrementa la altura en 5 pixeles
                panelPuerta.Top++; // baja la puerta 5 pixeles
            }
            else
            {
                tmrAbrir.Enabled = false;
            }
        }

        private void tmrCerrar_Tick(object sender, EventArgs e)
        {
            /* Este es el evento del botón de cerrar la puerta.
             * Lo que hace es incrementar la altura de la puerta en 5 pixeles y 
             * subirla en el eje Y 5 pixeles también
             */
            if (panelPuerta.Height < 165)
            {
                panelPuerta.Height++;
                panelPuerta.Top--;
            }
            else
            {
                tmrCerrar.Enabled = false;
            }
        }
    }
}
